//============================================================================
// Name        : Kth_max_element.cpp
// Author      : Shweta Goilkar
// Description : Find Kth min/max element in array
//============================================================================

#include <iostream>
#include <algorithm>
using namespace std;

void findKthMinMax(int arr[],int size,int pos){
	sort(arr,arr+size);
	cout<<"Kth max element is "<<arr[pos-1]<<endl;
	cout<<"kth min element is "<<arr[size-pos]<<endl;
}

int main() {
	int size, element,pos;
	cout << "Enter how many elements you want to add...";
	cin >> size;
	int arr[size];
	cout << "Enter the elements : ";
	for (int i = 0; i < size; i++) {
		cin >> element;
		arr[i] = element;
	}
	cout << "enter the kth position for max and min : ";
	cin >> pos;

	cout << "Given array is : " << endl;
		for (int j = 0; j < size; j++)
			cout << arr[j] << " ";
		cout << endl;


	findKthMinMax(arr,size,pos);
	return 0;
}
