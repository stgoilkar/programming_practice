//============================================================================
// Name        : Max_min_in_Array.cpp
// Author      : Shweta Goilkar
// Description : To find max and min element in array
//============================================================================

#include <iostream>
using namespace std;

void max_element(int arr[],int size){
	int max=arr[0];
	int min=arr[0];
	for(int i=0;i<size;i++){
		if(arr[i]>max)
			max=arr[i];
		if(arr[i]<min)
			min=arr[i];
	}
	cout<<"Maximum element in array is "<<max<<endl;
	cout<<"Minimum element in array is "<<min<<endl;
}

int main() {
	int size, element;
	cout << "Enter how many elements you want to add...";
	cin >> size;
	int arr[size];
	cout << "Enter the elements : ";
	for (int i = 0; i < size; i++) {
		cin >> element;
		arr[i] = element;
	}
	cout << "Given array is : " << endl;
	for (int j = 0; j < size; j++)
		cout << arr[j] << " ";
	cout<<endl;
	max_element(arr,size);
	return 0;
}
