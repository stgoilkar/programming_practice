//============================================================================
// Name        : Reverse_the_Array.cpp
// Author      : Shweta Goilkar
// Description : Reverse the Array
//============================================================================

#include <iostream>
using namespace std;


void reverse_array(int arr[],int size){
	for(int i=0;i<size/2;i++){
		int element=arr[size-i-1];
		arr[size-i-1]=arr[i];
		arr[i]=element;
	}
}

int main() {
	int size,element;
	cout<<"Enter how many elements you want to add...";
	cin>>size;
	int arr[size];
	cout<<"Enter the elements : ";
	for(int i=0;i<size;i++){
		cin>>element;
		arr[i]=element;
	}
	cout<<"Given array is : "<<endl;
	for(int j=0;j<size;j++)
		cout<<arr[j]<<" ";

	reverse_array(arr,size);
	cout<<"\nArray in reverse : "<<endl;
	for(int i=0;i<size;i++)
		cout<<arr[i]<<" ";
	return 0;
}
